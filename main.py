import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.metrics
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


# Single Feature
x=np.arange(0,30)
y=[3, 4, 5, 7, 10, 8, 9, 10, 10, 23, 27, 44, 50, 63, 67, 60, 62, 70, 75, 88, 81, 87, 95, 100, 108, 135, 151, 160, 169, 179]
# Plot data
plt.figure(figsize=(10,6))
plt.scatter(x,y)
plt.show()

# create polynomial features instance /// bias=false cause linear regression model will take care of this
poly=PolynomialFeatures(degree=2,include_bias=False)
poly_features=poly.fit_transform(x.reshape(-1,1))
poly_reg_model=LinearRegression()
# train model
poly_reg_model.fit(poly_features,y)
# start_predicting
y_predict=poly_reg_model.predict(poly_features)
print(f"original data{y} predicted data {y_predict}")
# plot model
plt.figure(figsize=(10,6))
plt.title("polynomial regression",size=16)
plt.scatter(x,y)
plt.plot(x,y_predict,c="red")
plt.show()

# Multiple Features

# So we can work with the same random data
np.random.seed(1)
x_1 = np.absolute(np.random.randn(100, 1) * 10)
x_2 = np.absolute(np.random.randn(100, 1) * 30)
y = 2*x_1**2 + 3*x_1 + 2 + np.random.randn(100, 1)*20
# Plot each feature on its own
fig,axes=plt.subplots(nrows=1,ncols=2,figsize=(10,4))
axes[0].scatter(x_1,y)
axes[1].scatter(x_2,y)
axes[0].set_title("x_1 plotted")
axes[1].set_title("x_2 plotted")
plt.show()
# create dataframe of the features so we can manipulate data easily
df=pd.DataFrame({"x_1":x_1.reshape(100,),"x_2":x_2.reshape(100,),"y":y.reshape(100,)},index=range(0,100))

X,y=df[["x_1","x_2"]],df["y"]
poly=PolynomialFeatures(degree=2,include_bias=False)
poly_features=poly.fit_transform(X)
X_train,X_test,y_train,y_test=train_test_split(poly_features,y,test_size=0.3,random_state=42)
# polynomial prediction model
poly_reg_model=LinearRegression()
poly_reg_model.fit(X_train,y_train)
poly_y_predict=poly_reg_model.predict(X_test)
poly_rmse=np.sqrt(mean_squared_error(y_test,poly_y_predict))
print(f"poly rmse= {poly_rmse}")
print(f"poly mode= {poly_reg_model.coef_}")

# Linear prediction model
X_train_lin,X_test_lin,y_train_lin,y_test_lin=train_test_split(X,y,test_size=0.3,random_state=42)
lin_reg_model=LinearRegression()
lin_reg_model.fit(X_train_lin,y_train_lin)
lin_y_predict=lin_reg_model.predict(X_test_lin)
lin_rmse=np.sqrt(mean_squared_error(y_test_lin,lin_y_predict))
print(f"linear rmse= {lin_rmse}")
print(f"linear model= {lin_reg_model.coef_}")